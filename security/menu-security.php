<div class="collapse navbar-collapse" id="app-navbar-collapse">
    <!-- Left Side Of Navbar -->
    <ul class="nav navbar-nav">
        <li<?php if($thisPage === "dashboard") echo ""; ?>><a href="../security/index.php">Dashboard</a></li>
        <li<?php if($thisPage === "vstatus") echo ""; ?>><a href="vstatus.php">Visitor Status</a></li>
        <li<?php if($thisPage === "vonsite") echo ""; ?>><a href="vonsite.php">On Site Visitor</a></li>
        <li<?php if($thisPage === "vleave") echo ""; ?>><a href="vleave.php">Leave Visitor</a></li>
        <li<?php if($thisPage === "vasing") echo ""; ?>><a href="vasing.php">Visitor WNA</a></li>
    </ul>

    <!-- Right Side Of Navbar -->
    <ul class="nav navbar-nav navbar-right">
        <!-- Authentication Links
            <li><a href="#">Login</a></li>
            <li><a href="#">Daftar</a></li> -->
    </ul>
</div>