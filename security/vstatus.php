<?php $thisPage = "vstatus"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->

    <title><?php $thisPage ?></title>

    <!-- Styles -->
    <link href="../css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!--<link href="../css/jquery.dataTables.css" rel="stylesheet"> -->
    <link href="../css/dataTables.bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="#">
                    AVCMS
                </a>
            </div>

            <?php
            // menu navigasi
            include "menu-security.php";
            ?>

        </div>
    </nav>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="../security/index.php">Dashboard</a></li>
                <li class="active">Visitor Status</li>
            </ul>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">Monitor status Tamu</h2>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">

                            <tr>
                                <th>V.NO.</th>
                                <th>VISITOR</th>
                                <th>COMPANY</th>
                                <th>MEET</th>
                                <th>PURPOSE</th>
                                <th>PLANT</th>
                                <th>IN</th>
                                <th>OUT</th>
                                <th>DURATION</th>
                                <th>STATUS</th>
                            </tr>

                            <?php
                            include('../koneksi.php');
                            $tanggal = date('Y-m-d h:m:s');
                            echo $tanggal;
                            $qry = "SELECT * FROM logs";
                            $check = $db->query($qry) or die($db->error.__LINE__);
                            if($check -> num_rows > 0){

                                $sql = "SELECT
                            logs.id,
                            logs.name AS visitor,
                            logs.company,
                            staff.name AS staff,
                            logs.purposes,
                            logs.plan,
                            logs.chkin,
                            logs.chkout,
                            TIMEDIFF(chkout,chkin) AS durasi,
                            logs.plan,
                            logs.status
                          FROM
                            logs
                          INNER JOIN staff ON logs.tomeet = staff.nik
                          WHERE
                            status = 1
                          ORDER BY
                            logs.status DESC,
                            logs.id DESC ";

                                if(!$result = $db->query($sql)){
                                    die('Query error [' .$db->error . ']');
                                }

                                while($tamu = $result->fetch_object()){
                                    echo "<tr>";
                                    echo "<td>".$tamu->id."</td>";
                                    echo "<td>".$tamu->visitor."</td>";
                                    echo "<td>".$tamu->company."</td>";
                                    echo "<td>".strtoupper($tamu->staff)."</td>";
                                    echo "<td>".$tamu->purposes."</td>";
                                    echo "<td>".$tamu->plan."</td>";
                                    echo "<td>".$tamu->chkin."</td>";
                                    echo "<td>".$tamu->chkout."</td>";
                                    echo "<td>".$tamu->durasi."</td>";
                                    if($tamu->status == 0){
                                        $status = "Leave";
                                    }else{
                                        $status = "Onsite";
                                    }
                                    echo "<td>";
                                    if($status == "Leave"){
                                        echo '<p style="color: green">'.$status.'</p>';
                                    }else{
                                        echo '<p style="color: red">'.$status.'</p>';
                                    }
                                    echo "</td>";
                                    echo "</tr>";
                                }

                            }else{
                                echo "<tr>";
                                echo '<td colspan="10" align="center">';
                                echo '<p style="\color: red;\">Data belum tersedia</p>';
                                echo "</td>";
                                echo "</tr>";
                            }
                            ?>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="../js/jquery-1.12.4.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.dataTables.min.js"></script>
<script src="../js/dataTables.bootstrap.min.js"></script>
</body>
</html>