
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>AVCMS 3</title>

    <!-- Styles -->
    <!--<link href="/css/app.css" rel="stylesheet"> -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="css/selectize.css" rel="stylesheet">
    <link href="css/selectize.bootstrap3.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="index.html">
                         Back
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>
                    <!-- Right Side Of Navbar -->
                    
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-md-offset-0">
                    <div class="panel panel-default">
                        <div class="panel-heading">Booking Visitors</div>

                        <div class="panel-body">
                            <p>Tamu Asing yang datang pada hari ini tanggal <u><strong><?php echo date("d M Y") ?></strong></u> adalah sebagai berikut:</p>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <tr>
                                        <th>No.</th><th>Tgl. Visit</th><th>Nama Tamu</th><th>Bertemu dengan</th><th>Keperluan</th><th>&nbsp;</th>
                                    </tr>
                                    <tr>
                                        <?php
                                            include('koneksi.php');
                                            $date = date("Y-m-d");
                                            $qry = "SELECT * FROM booking WHERE visit_date='$date'";
                                            $check = $db->query($qry) or die($db->error.__LINE__);  
                                            
                                            if($check -> num_rows > 0){
                                                $sql = "SELECT
                                                        b.id,
                                                        b.visit_date,
                                                        b.`name`,
                                                        b.company,
                                                        b.to_meet,
                                                        b.purpose,
                                                        s.`name` as staff_name,
                                                        s.nik
                                                    FROM
                                                        booking AS b ,
                                                        staff AS s
                                                    WHERE
                                                        b.to_meet = s.nik
                                                    AND 
                                                        visit_date='$date'";
                                                if(!$result = $db->query($sql)){
                                                    die('Query error [' .$db->error . ']');
                                                }
                                                
                                                $no = 1;
                                                while($booking = $result->fetch_object()){
                                                    
                                                        echo "<tr>";
                                                            echo "<td>".$no."</td>";
                                                            echo "<td>".$booking->visit_date ."</td>";
                                                            echo "<td>".$booking->name."</td>";
                                                            echo "<td>".$booking->staff_name ."</td>";
                                                            echo "<td>".$booking->purpose ."</td>";
                                                            echo "<td align=\"center\">
                                                                    <a href=\"booking-checkin.php?id=$booking->id\" class=\"btn btn-sm btn-warning\"><span class=\"glyphicon glyphicon-ok\"></span></a>
                                                                 </td>";
                                                        echo "</tr>";
                                                   
                                                    $no++;
                                                }

                                            }else{
                                                echo "<tr>";
                                                echo "<td colspan=\"10\" align=\"center\">";
                                                echo "<p style=\"color:red\">Belum ada tamu pada hari ini</p>";
                                                echo "</td>";
                                                echo "</tr>";   
                                            }
                                        ?>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Scripts -->
    <script src="js/selectize.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/app.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/app.js"></script>
</body>
</html>