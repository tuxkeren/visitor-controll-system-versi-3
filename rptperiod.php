<!DOCTYPE html>
<html>
<head>
    <link href="metro-ui/build/css/metro.css" rel="stylesheet">
    <link href="metro-ui/build/css/metro-icons.css" rel="stylesheet">
    <script src="metro-ui/build/js/jquery.js"></script>
    <script src="metro-ui/build/js/metro.js"></script>
</head>
<body>
    <header>
        <?php include("menu.php"); ?>
    </header>
    <div class="container-table"> 
       	<div>
            <h1><a href="dash.php" class="nav-button transform"><span></span></a>&nbsp;Dashboard</h1>
            <h3>Report Visitor by Period</h3>
        </div>
        <form action="procrptperiod.php" method="post">
        <table>
            <tr><td> Please entry Start Date:</td><td><input type="date" name="sdate" placeholder="YYYY-MM-DD" /></td>
                <td>&nbsp;</td>
                <td> Please entry End Date:</td><td><input type="date" name="edate" placeholder="YYYY-MM-DD" /></td>
                <td>&nbsp;</td>
                <td><button class="button danger" type="submit" name="show" />Show Data</button>
            </tr>
            <table class="table striped hovered border bordered" border="0">
                <tr>
                    <th>Visit No.</th>
                    <th>Visitor</th>
                    <th>Company</th>
                    <th>Meet</th>
                    <th>Purpose</th>
                    <th>Plan</th>
                    <th>Time In</th>
                    <th>Time Out</th>                  
                </tr>
                <tr>
                    <td colspan="8" align="center">No Data Show, Please select entry Start Date and End Date above</td>
                </tr>   
            </table>
            <br/><br/>
        </form>
    <footer>
        <?php include("footer.php"); ?>
    </footer>
</body>
</html>