<!DOCTYPE html>
<!DOCTYPE html>
<html>
<head>
    <!--<link href="metro-ui/build/css/metro.css" rel="stylesheet"> -->
    <link href="print.css" rel="stylesheet">
</head>
<body>
    <?php
        include 'koneksi.php';

        $sql = "SELECT id, name, company, plan FROM logs WHERE status=1 ORDER BY id DESC LIMIT 1";


          if(!$result = $db->query($sql)){
              die('query error [' . $db->error . ']');

          }

    ?>
    <div  class="">
        <?php while($guest = $result->fetch_object()){ ?>
        <table border="0" width="280px">
            <tr>
                <th align="center"><h1>VISITOR CARD</h1><hr></th>
            </tr>
            <tr>
                <td align="center" height="300px" valign="bottom">
                    <table width="270" border="0">
                        <tr>
                            <td align="center">
                                <strong><i>Name:</i></strong>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <h2><?php echo ucwords($guest->name); ?></h2>
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                            <td align="center"><strong><i>Company:</i></strong></td>
                        </tr>
                        <tr>
                            <td align="center"><h2><?php echo ucwords($guest->company); ?><h2></td>
                        </tr>
                         <tr><td>&nbsp;</td></tr>
                        <tr>
                            <td align="center"><strong><i>Visit No:</i></strong></td>
                        </tr>
                        <tr>
                            <td align="center"><font size="20px"><?php echo $guest->id; ?></font></td>
                        </tr>

                        <tr>
                            <td align="center"><strong><i>Visit Plan:</i></strong></td>
                        </tr>
                        <tr>
                            <td align="center"><strong><?php echo $guest->plan; ?></strong></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <div class="barcode">
                                    <?php echo '*'.$guest->id.'*'; ?>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <?php }; ?>
            <br/>
        </table>
    </div>
    <button type="button" name="cetak" onclick="javascript:window.print()">
        <a href="#">P</a>
    </button>
      <a href="index.html">X</a>
</body>
</html>
