<?php $thisPage = "rpt-asingperpriode"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->

    <title>Dashboard</title>

    <!-- Styles -->
    <link href="../css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/jquery.dataTables.css" rel="stylesheet">
    <link href="../css/dataTables.bootstrap.css" rel="stylesheet">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="#">
                    AVCMS
                </a>
            </div>

            <?php
            // menu navigasi
            include "../admin/menu-navigasi.php";
            ?>

        </div>
    </nav>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="../admin/index.php">Dashboard</a></li>
                <li><a href="#">Laporan</a></li>
                <li class="active">Tamu Asing Per Periode</li>
            </ul>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">Laporan Tamu Asing Per Periode</h2>
                </div>
                <div class="panel-body">
                    <div class="row col-md-12">
                        <div class="table-responsive col-md-12">
                            <form action="proses-rpt-asing-perpriode.php" method="post">
                                <div class="col-md-12">
                                    <div class="col-lg-2">
                                        &nbsp;
                                    </div>
                                    <div class="col-lg-4" align="center">
                                        <p>Tanggal Mulai:</p>
                                        <input type="date" class="form-control" name="sdate" placeholder="YYYY-MM-DD" required>
                                    </div>
                                    <div class="col-lg-4" align="center">
                                        <p>Tanggal Akhir: </p>
                                        <input type="date" class="form-control" name="edate" placeholder="YYYY-MM-DD" required>
                                    </div>
                                    <div class="col-lg-2">
                                        &nbsp;
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    &nbsp;
                                </div>
                                <div class="col-md-12">
                                    <div class="col-lg-4">
                                        &nbsp;
                                    </div>
                                    <div class="col-lg-4" align="center">
                                        <button class="btn btn-sm-primary" type="submit">Proses</button>
                                    </div>
                                    <div class="col-lg-4">
                                        &nbsp;
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Scripts -->
<script src="../js/jquery-3.1.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.dataTables.min.js"></script>
<script src="../js/dataTables.bootstrap.min.js"></script>
</body>
</html>
