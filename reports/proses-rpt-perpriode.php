<?php
    session_start();
    $thisPage = "rpt-onsite";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->

    <title><?php $thisPage ?></title>

    <!-- Styles -->
    <link href="../css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!--<link href="../css/jquery.dataTables.css" rel="stylesheet"> -->
    <link href="../css/dataTables.bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="#">
                    AVCMS
                </a>
            </div>

            <?php
            // menu navigasi
            include "../admin/menu-navigasi.php";
            ?>

        </div>
    </nav>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="../admin/index.php">Dashboard</a></li>
                <li><a href="#">Laporan</a></li>
                <li><a href="../reports/rpt-perpriode.php">Per Periode</a></li>
                <li class="active">Preview</li>
            </ul>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">Laporan On Site Visitor</h2>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <?php
                            $sdate = $_POST['sdate'];
                            $edate = $_POST['edate'];

                            $_SESSION['sdate'] = $sdate;
                            $_SESSION['edate'] = $edate;
                        ?>

                        <p align="center"style="font-size: 20px">
                            <strong>LAPORAN SEMUA VISITOR PER PERIODE</strong>
                        </p>
                        <p align="center"style="font-size: 18px">
                            <strong><?php echo $sdate; ?> SAMPAI <?php echo $edate; ?></strong>
                        </p>
                        <p>&nbsp;</p>
                        <table class="table table-bordered">
                            <tr>
                                <th>V. NO.</th>
                                <th>VISITOR</th>
                                <th>COMPANY</th>
                                <th>MEET</th>
                                <th>PURPOSE</th>
                                <th>PLANT</th>
                                <th>TIME IN</th>
                                <th>TIME OUT</th>
                            </tr>

                            <?php
                                include('../koneksi.php');

                                $qry = "SELECT * FROM logs WHERE status=0";
                                $check = $db->query($qry) or die($db->error.__LINE__);
                                if($check -> num_rows > 0){

                                    $sql = "SELECT
                                        logs.id,
                                        logs.name AS visitor,
                                        logs.company,
                                        logs.telephone,
                                        Staff.name AS staff,
                                        logs.purposes,
                                        logs.plan,
                                        logs.chkin,
                                        logs.chkout,
                                        logs.voa
                                    FROM
                                        logs
                                    INNER JOIN staff AS Staff ON Staff.nik = logs.tomeet
                                    WHERE
                                        logs.chkin >= '$sdate 00:00:00' AND
                                        logs.chkin <= '$edate 00:00:00'
                                    ORDER BY
                                        logs.id ASC";

                                    if(!$result = $db->query($sql)){
                                        die('Query error [' .$db->error . ']');
                                    }

                                    while($tamu = $result->fetch_object()){
                                        echo "<tr>";
                                        echo "<td>".$tamu->id."</td>";
                                        echo "<td>".$tamu->visitor."</td>";
                                        echo "<td>".$tamu->company."</td>";
                                        echo "<td>".strtoupper($tamu->staff)."</td>";
                                        echo "<td>".$tamu->purposes."</td>";
                                        echo "<td>".$tamu->plan."</td>";
                                        echo "<td>".$tamu->chkin."</td>";
                                        echo "<td>".$tamu->chkout."</td>";
                                        echo "</tr>";
                                    }
                                }else{
                                    echo "<tr>";
                                    echo '<td colspan="10" align="center">';
                                    echo '<p style="color: red">Data belum tersedia</p>';
                                    echo "</td>";
                                    echo "</tr>";
                                }

                            ?>
                        </table>
                        <a href="pdf-rpt-per-periode.php" class="btn btn-primary">Export ke PDF</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="../js/jquery-1.12.4.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.dataTables.min.js"></script>
<script src="../js/dataTables.bootstrap.min.js"></script>
</body>
</html>