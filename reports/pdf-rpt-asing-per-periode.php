<?php
session_start();
$sdate = $_SESSION['sdate'];
$edate = $_SESSION['edate'];

include('../koneksi.php');

$sql = "SELECT
            logs.id,
            logs.name AS visitor,
            logs.company,
            logs.idcardno,
            logs.voa,
            logs.telephone,
            Staff.name AS staff,
            logs.purposes,
            logs.plan,
            logs.chkin,
            logs.chkout,
            logs.voa
        FROM
            logs
        INNER JOIN staff AS Staff ON Staff.nik = logs.tomeet
        WHERE
            logs.chkin >= '$sdate 00:00:00' AND
            logs.chkin <= '$edate 00:00:00' AND
            logs.voa   <> ''
        ORDER BY
            logs.id ASC";

require('../fpdf/fpdf.php');

$pdf = new FPDF('L','mm','A4');
$pdf->AddPage();

// Judul Laporan
$pdf->SetFont('Arial','B',14);
$pdf->Text(10,10, 'LAPORAN VISITOR ASING PER PERIODE');
$pdf->SetFont('Arial','B',10);
$pdf->Text(10,16, 'DARI: '.$sdate .' SAMPAI: '.$edate);
$pdf->Text(10,20,'');

//Tabel Header
$pdf->SetFont('Arial','B','C');
$pdf->SetFontSize(8);
$pdf->Cell(10, 10,'', '', 1);
$pdf->Cell(14, 6,'V.NO', 1, 0);
$pdf->Cell(43, 6,'VISITOR NAME', 1, 0);
$pdf->Cell(58, 6,'COMPANY', 1, 0);
$pdf->Cell(20, 6,'PASPOR', 1, 0);
$pdf->Cell(20, 6,'VOA NO.', 1, 0);
$pdf->Cell(30, 6,'MEET', 1, 0);
$pdf->Cell(25, 6,'PURPOSE', 1, 0);
$pdf->Cell(12, 6,'PLANT', 1, 0);
$pdf->Cell(30, 6,'TIME IN', 1, 0);
$pdf->Cell(30, 6,'TIME OUT', 1, 1);

// Data tabel
$pdf->SetFont('Helvetica','','C');
if(!$result = $db->query($sql)){
    die('Query error [' .$db->error . ']');
}

while($tamu = $result->fetch_object()) {
    $pdf->Cell(14, 6,$tamu->id , 1, 0);
    $pdf->Cell(43, 6,$tamu->visitor, 1, 0);
    $pdf->Cell(58, 6,$tamu->company, 1, 0);
    $pdf->Cell(20, 6,$tamu->idcardno, 1, 0);
    $pdf->Cell(20, 6,$tamu->voa, 1, 0);
    $pdf->Cell(30, 6,strtoupper($tamu->staff), 1, 0);
    $pdf->Cell(25, 6,$tamu->purposes, 1, 0);
    $pdf->Cell(12, 6,$tamu->plan, 1, 0);
    $pdf->Cell(30, 6,$tamu->chkin, 1, 0);
    $pdf->Cell(30, 6,$tamu->chkout, 1, 1);
}
$pdf->Output();