# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Database: avms
# Generation Time: 2017-02-25 15:13:20 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table booking
# ------------------------------------------------------------

CREATE TABLE `booking` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `visit_date` date NOT NULL,
  `name` varchar(55) NOT NULL DEFAULT '',
  `company` varchar(55) DEFAULT NULL,
  `to_meet` varchar(55) NOT NULL DEFAULT '',
  `purpose` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `booking` WRITE;
/*!40000 ALTER TABLE `booking` DISABLE KEYS */;

INSERT INTO `booking` (`id`, `visit_date`, `name`, `company`, `to_meet`, `purpose`)
VALUES
	(1,'2017-02-26','Nelson Chen Yaosheng','NOV Singapore','Patrick','Meeting'),
	(2,'2017-04-28','Martin John Kinsella ','Insitu UK','Slamet Heriyanto ','HES Award');

/*!40000 ALTER TABLE `booking` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fguest
# ------------------------------------------------------------

CREATE TABLE `fguest` (
  `guest_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `place_birth` varchar(50) NOT NULL,
  `date_birth` date NOT NULL,
  `nationality` varchar(50) NOT NULL,
  `passport_no` varchar(50) NOT NULL,
  `date_issue` date NOT NULL,
  `date_expiry` date NOT NULL,
  `company` varchar(50) DEFAULT NULL,
  `passport_foto` varchar(255) DEFAULT NULL,
  `note` text,
  PRIMARY KEY (`guest_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `fguest` WRITE;
/*!40000 ALTER TABLE `fguest` DISABLE KEYS */;

INSERT INTO `fguest` (`guest_id`, `name`, `place_birth`, `date_birth`, `nationality`, `passport_no`, `date_issue`, `date_expiry`, `company`, `passport_foto`, `note`)
VALUES
	(5,'Nelson Chen Yaosheng','Singapore','1983-10-06','Singapore','E4184763F','2013-10-21','2019-07-21','NOV Excel Singapore','SG0001365.png',''),
	(6,'Martin John Kinsella','Britain','1959-08-05','Great Britain','534981882','2017-08-10','2017-02-10','NOV ','A3517263.png','');

/*!40000 ALTER TABLE `fguest` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table logs
# ------------------------------------------------------------

CREATE TABLE `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idcardtype` varchar(30) NOT NULL,
  `idcardno` varchar(30) NOT NULL,
  `name` varchar(50) NOT NULL,
  `company` varchar(50) NOT NULL,
  `voa` varchar(50) DEFAULT NULL,
  `telephone` varchar(50) DEFAULT NULL,
  `tomeet` varchar(50) NOT NULL,
  `purposes` text NOT NULL,
  `chkin` datetime NOT NULL,
  `chkout` datetime DEFAULT NULL,
  `plan` varchar(10) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `passport_foto` varchar(50) DEFAULT NULL,
  `voa_foto` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `staff` (`tomeet`),
  CONSTRAINT `staff` FOREIGN KEY (`tomeet`) REFERENCES `staff` (`nik`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;

INSERT INTO `logs` (`id`, `idcardtype`, `idcardno`, `name`, `company`, `voa`, `telephone`, `tomeet`, `purposes`, `chkin`, `chkout`, `plan`, `status`, `passport_foto`, `voa_foto`)
VALUES
	(4,'PASPOR','A1234234','JAMES RIADY','PODOMORO GROUP','','','120301','MEETING WARUNG MAKAN.','2017-02-25 18:54:48','2017-02-25 20:31:41','XL',0,NULL,NULL),
	(7,'PASPOR','W24531','LIM SIOE LONG','BCA','-','021-333212345','980122','MEMBAHAS MASALAH KEBON SAWIT.','2017-02-25 19:10:22','2017-02-25 20:32:07','XL',0,NULL,NULL),
	(9,'KK','21324234','MUZANNI','MADU MURNI MUZANNI','','','980122','DAGANG MADU','2017-02-25 19:33:58','2017-02-25 20:32:14','DP',0,NULL,NULL);

/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table staff
# ------------------------------------------------------------

CREATE TABLE `staff` (
  `nik` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `desegnation` varchar(50) NOT NULL,
  `department` varchar(30) NOT NULL,
  `mobileext` varchar(30) DEFAULT NULL,
  `photo` blob,
  PRIMARY KEY (`nik`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `staff` WRITE;
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;

INSERT INTO `staff` (`nik`, `name`, `desegnation`, `department`, `mobileext`, `photo`)
VALUES
	('000000','Patrick O\'Reilly','VM Manufacturing','Management','30',NULL),
	('000001','Dustann Sapp','Manufacturing Engineer','XL','16',NULL),
	('000129','Sudarmanto','XL Maintenance','Maintenance','23',NULL),
	('041001','Sapadan Saragih','Accounting Officer','Finance','27',NULL),
	('060516','Ludhfie Yusnan Yusuf','XL HR','General','10',NULL),
	('080505','Andriyanto','PPIC & Technical Officer','General','15',NULL),
	('081222','Athailah','IT System Administrator','General','14',NULL),
	('110711','Dani Taufik Sukandi','HSE Officer','HSE','17',NULL),
	('110822','Rahman Hadi','QA Officer','QA','11',NULL),
	('120301','Slamet Heriyanto','General Manager','General','18',NULL),
	('130819','Zukhairi','QA Manager','QA','12',NULL),
	('130820','Agung Sutrisno','XL Maintenance','Maintenance','23',NULL),
	('140912','Hengky Irawan','XL Logistik Supervisor','Logistic','32',NULL),
	('840040','Jonder Gultom','XL Production Manager','Production','14',NULL),
	('940975','Joniel Saragih','Accounting Supervisor','Finance','23',NULL),
	('970911','Mujiono','Maintenance Supervisor','Maintenance','13',NULL),
	('970914','Susilo Swiwidodo','Purchasing Officer','Purchasing','10',NULL),
	('980122','Ribut Hariyanto','Purchasing Supervisor','Purchasing','20',NULL),
	('980626','Agus Purwati','GA Supervisor','HR','29',NULL);

/*!40000 ALTER TABLE `staff` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
