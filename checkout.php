
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>AVCMS 3</title>

    <!-- Styles -->
    <!--<link href="/css/app.css" rel="stylesheet"> -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="css/selectize.css" rel="stylesheet">
    <link href="css/selectize.bootstrap3.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="index.html">
                         Back
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>
                    <!-- Right Side Of Navbar -->
                    
                </div>
            </div>
        </nav>
        <div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading" align="center">
                	<h4>Please enter your visit number in the box below, then click OK.</h4>
                	<h4><i>Silahkan masukkan nomor kunjungan Anda dalam kotak dibawah, dan kemudian klik OK.</i>
                </div>

                <div class="panel-body">
                    <form method="POST" action="procout.php" accept-charset="UTF-8" class="form-horizontal">

						<div class="form-group">
							<label for="visitno" class="col-md-4 control-label">Visit Number:</label>
								<div class="col-md-6">
									<input class="form-control" name="visitno" type="number" id="visitno" required="yes">		
								</div>
						</div>
						
						<div>&nbsp;</div>

						<div class="form-group">
							<div class="col-md-4 col-md-offset-4">
								<input class="btn btn-success" type="submit" value="Check in">
							</div>
						</div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- Scripts -->
    <script src="js/selectize.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/app.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/app.js"></script>
</body>
</html>