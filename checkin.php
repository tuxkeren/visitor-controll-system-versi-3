
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>AVCMS 3</title>

    <!-- Styles -->
    <!--<link href="/css/app.css" rel="stylesheet"> -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="css/selectize.css" rel="stylesheet">
    <link href="css/selectize.bootstrap3.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="index.html">
                         Back
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>
                    <!-- Right Side Of Navbar -->
                    
                </div>
            </div>
        </nav>
        <div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">Please complete the form</div>

                <div class="panel-body">
                    <form method="POST" action="procin.php" accept-charset="UTF-8" class="form-horizontal">

						<div class="form-group">
							<label for="type_id" class="col-md-4 control-label">Type ID:</label>
								<div class="col-md-6">
									<select class="form-control" id="type_id" name="type_id" required="yes">
										<option value="" selected="selected"></option>
										<option value="KTP">KTP</option>
										<option value="Paspor">Paspor</option>
										<option value="SIM">SIM</option>
										<option value="Badge Perusahaan">KK</option>
									</select>
								</div>
						</div>

						<div class="form-group">
							<label for="no_id" class="col-md-4 control-label">No. ID:</label>
								<div class="col-md-6">
									<input class="form-control" name="no_id" type="text" id="no_id" required="yes">
									
								</div>
						</div>

						<div class="form-group">
							<label for="full_name" class="col-md-4 control-label">Full Name (Nama Lengkap):</label>
								<div class="col-md-6">
									<input class="form-control" name="full_name" type="text" id="full_name" required="yes">
									
								</div>
						</div>

						<div class="form-group">
							<label for="company" class="col-md-4 control-label">From Company (Dari Perusahaan):</label>
								<div class="col-md-6">
									<input class="form-control" name="company" type="text" id="company" required="yes">
									
								</div>
						</div>

						<div class="form-group">
							<label for="voa" class="col-md-4 control-label">Visa On Arrival No [Must entry for Foreigner]:</label>
								<div class="col-md-6">
									<input class="form-control" name="voa" type="text" id="voa">
									
								</div>
						</div>

						<div class="form-group">
							<label for="telepon" class="col-md-4 control-label">Telepon:</label>
								<div class="col-md-6">
									<input class="form-control" name="telepon" type="text" id="telepon">
									
								</div>
						</div>

						<div class="form-group">
							<label for="to_meet" class="col-md-4 control-label">To Meet [Ingin bertemu dengan]:</label>
								<div class="col-md-6">
									<select class="form-control" id="to_meet" name="to_meet" required="yes">
										<?php 
											include 'koneksi.php';
                                			$sql = "SELECT * FROM staff ORDER BY name ASC";
                                			if(!$result = $db->query($sql)){
                                    			die('query error ['. $db->error.']');
                                			}
                                			while ($staff = $result->fetch_object()) {
                                		?>
                                		<option value="" selected="selected"></option>
                            			<option value="<?php echo strtoupper($staff->nik); ?>">
                                			<?php echo strtoupper($staff->name); ?>
                            			</option>
                            			<?php
                                			};
                                			$db->close();
                            			?>
									</select>
								</div>
						</div>

						<div class="form-group">
							<label for="purpose" class="col-md-4 control-label">Purpose [Keperluan]:</label>
								<div class="col-md-6">
									<input class="form-control" name="purpose" type="text" id="purpose" required="yes">
									
								</div>
						</div>

						<div class="form-group">
							<label for="plant" class="col-md-4 control-label">Plant:</label>
								<div class="col-md-6">
									<select class="form-control" id="plant" name="plant" required="yes">
										<option value="" selected="selected"></option>
										<option value="XL">XL Plant</option>
										<option value="DP">DP Plant</option>
										<option value="WH">Warehouse</option>
									</select>
									
								</div>
						</div>
						
						<div class="form-group">
							<label for="time_in" class="col-md-4 control-label">Time in:</label>
								<div class="col-md-6">
								 	<?php
                        				date_default_timezone_set("Asia/Jakarta");
                        				$time = date('Y-m-d H:i:s');
                    				?>
									<input class="form-control" name="time_in" type="text" value="<?php echo $time; ?>" id="time_in" readonly>
									
								</div>
						</div>

						<div class="form-group">
							<div class="col-md-4 col-md-offset-4">
								<input class="btn btn-success" type="submit" value="Check in">
								<input class="btn btn-danger" type="reset" value="Reset">
							</div>
						</div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- Scripts -->
    <script src="js/selectize.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/app.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/app.js"></script>
</body>
</html>