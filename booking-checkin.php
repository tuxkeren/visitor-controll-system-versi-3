
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>AVCMS 3</title>

    <!-- Styles -->
    <!--<link href="/css/app.css" rel="stylesheet"> -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/dataTables.bootstrap.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="index.html">
                         Back
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>
                    <!-- Right Side Of Navbar -->
                    
                </div>
            </div>
        </nav>
        <div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">Please complete the form</div>

                <?php
                    $id = $_GET['id'];

                    include('koneksi.php');

                    $qry = "SELECT * FROM booking";
                    $check = $db->query($qry) or die($db->error.__LINE__);	
                    if($check -> num_rows > 0){
                    $sql = "SELECT
                                b.id,
                                b.visit_date,
                                b.`name`,
                                b.company,
                                b.to_meet,
                                b.purpose,
                                s.`name` as staff_name,
                                s.nik
                            FROM
                                booking AS b ,
                                staff AS s
                            WHERE
                                b.to_meet = s.nik
                            AND 
                                b.id='$id'";

                        if(!$result = $db->query($sql)){
                            die('Query error [' .$db->error . ']');
                        }

                        while($booking = $result->fetch_object()){

                ?>

                <div class="panel-body">
                    <form method="POST" action="booking-procin.php" accept-charset="UTF-8" class="form-horizontal">
                        <div>
                            <input type="hidden" name="id" value="<?php echo $id ?>">
                        </div>
						<div class="form-group">
							<label for="full_name" class="col-md-4 control-label">Full Name (Nama Lengkap):</label>
								<div class="col-md-6">
									<strong><input class="form-control" name="full_name" type="text" id="full_name" value="<?php echo $booking->name; ?>" readonly></strong>
									
								</div>
						</div>
						
						<div class="form-group">
							<label for="company" class="col-md-4 control-label">From Company (Dari Perusahaan):</label>
								<div class="col-md-6">
									<strong><input class="form-control" name="company" type="text" id="company" value="<?php echo $booking->company; ?>" readonly></strong>
									
								</div>
						</div>
						
						<div class="form-group">
							<label for="to_meet" class="col-md-4 control-label">To Meet [Ingin bertemu dengan]:</label>
								<div class="col-md-2">
									<strong><input class="form-control" name="to_meet" type="text" value="<?php echo $booking->to_meet; ?>" id="to_meet" readonly></strong>
								</div>
                                <div class="col-md-6">
                                    <?php echo "<p style=\" font-size: 25px; color: firebrick\" >$booking->staff_name</p>"; ?>
                                </div>
						</div>

						<div class="form-group">
							<label for="purpose" class="col-md-4 control-label">Purpose [Keperluan]:</label>
								<div class="col-md-6">
									<strong><input class="form-control" name="purpose" type="text" value="<?php echo $booking->purpose; ?>" id="purpose" readonly></strong>
									
								</div>
						</div>
						
						<div class="form-group">
							<label for="time_in" class="col-md-4 control-label">Time in:</label>
								<div class="col-md-6">
								 	<?php
                        				date_default_timezone_set("Asia/Jakarta");
                        				$time = date('Y-m-d H:i:s');
                    				?>
									<strong><input class="form-control" name="time_in" type="text" value="<?php echo $time; ?>" id="time_in" readonly></strong>
									
								</div>
						</div>

						<div class="form-group">
							<label for="type_id" class="col-md-4 control-label">Type ID:</label>
								<div class="col-md-6">
									<strong><input class="form-control" name="type_id" type="text" id="type_id" value="Passport" readonly></strong>
								</div>
						</div>

						<div class="form-group">
							<label for="no_id" class="col-md-4 control-label">Passport No:</label>
								<div class="col-md-6">
									<input class="form-control" name="no_id" type="text" id="no_id" required="yes" autofocus>
									
								</div>
						</div>

						<div class="form-group">
							<label for="voa" class="col-md-4 control-label">VOA No:</label>
								<div class="col-md-6">
									<input class="form-control" name="voa" type="text" id="voa">
									
								</div>
						</div>

						<div class="form-group">
							<label for="telepon" class="col-md-4 control-label">Telepon:</label>
								<div class="col-md-6">
									<input class="form-control" name="telepon" type="text" id="telepon">
									
								</div>
						</div>

						<div class="form-group">
							<label for="plant" class="col-md-4 control-label">Plant:</label>
								<div class="col-md-6">
									<select class="form-control" id="plant" name="plant" required="yes">
										<option value="" selected="selected"></option>
										<option value="XL">XL Plant</option>
										<option value="DP">DP Plant</option>
										<option value="WH">Warehouse</option>
									</select>
									
								</div>
						</div>
					
						<div class="form-group">
							<div class="col-md-4 col-md-offset-4">
								<input class="btn btn-success" type="submit" value="Check in">
								<input class="btn btn-danger" type="reset" value="Reset">
							</div>
						</div>
				<?php
					}
				};
				?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- Scripts -->
    <script src="js/selectize.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/app.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/app.js"></script>
</body>
</html>