<!DOCTYPE html>
<html>
<head>
    <link href="metro-ui/build/css/metro.css" rel="stylesheet">
    <link href="metro-ui/build/css/metro-icons.css" rel="stylesheet">
    <script src="metro-ui/build/js/jquery.js"></script>
    <script src="metro-ui/build/js/metro.js"></script>
</head>
<body>
    <header>
        <?php include("menu.php"); ?>
    </header>
    <div class="container-table"> 
       	<div>
            <h1><a href="dash.php" class="nav-button transform"><span></span></a>&nbsp;Dashboard</h1>
        </div>
        <form action="procrptperiod.php" method="post">
        <table>
            <tr><td> Please entry Start Date:</td><td><input type="date" name="sdate" placeholder="YYYY-MM-DD" /></td>
                <td>&nbsp;</td>
                <td> Please entry End Date:</td><td><input type="date" name="edate" placeholder="YYYY-MM-DD" /></td>
                <td>&nbsp;</td>
                <td><button class="button danger" type="submit" name="show" />Show Data</button>
            </tr>
        </table>
        <h3>Report Visitor by Period</h3>
         <table class="table striped hovered border bordered" border="0">
                <tr>
                    <th>Visit No.</th>
                    <th>Visitor</th>
                    <th>Company</th>
                    <th>Meet</th>
                    <th>Purpose</th>
                    <th>Plan</th>
                    <th>Time In</th>
                    <th>Time Out</th>                  
                </tr>
		        <?php
		            $sdate = $_POST['sdate'];
		            $edate = $_POST['edate'];

		            include('koneksi.php');
					$qry = "SELECT * FROM logs WHERE status=0";
					$check = $db->query($qry) or die($db->error.__LINE__);	
					if($check -> num_rows > 0){

						$sql = "SELECT
									logs.id,
									logs.name AS visitor,
									logs.company,
									logs.telephone,
									Staff.name AS staff,
									logs.purposes,
									logs.plan,
									logs.chkin,
									logs.chkout,
									logs.voa
								FROM
									logs
								INNER JOIN staff AS Staff ON Staff.nik = logs.tomeet
								WHERE
									logs.chkin >= '$sdate 00:00:00' AND
									logs.chkin <= '$edate 00:00:00'
								ORDER BY
									logs.id ASC";

						if(!$result = $db->query($sql)){
		                        die('Query error [' .$db->error . ']');
		                    }

		                while($tamu = $result->fetch_object()){
			                	echo "<tr>";
			                		echo "<td>".$tamu->id."</td>";
			                		echo "<td>".$tamu->visitor."</td>";
			                		echo "<td>".$tamu->company."</td>";
			                		echo "<td>".strtoupper($tamu->staff)."</td>";
			                		echo "<td>".$tamu->purposes."</td>";
			                		echo "<td>".$tamu->plan."</td>";
			                		echo "<td>".$tamu->chkin."</td>";
			                		echo "<td>".$tamu->chkout."</td>";
								echo "</tr>";
			                }
		            }else{
					        	echo "<tr>";
					        	echo '<td colspan="10" align="center">';
					        		echo '<font color="red">Data belum tersedia</font>';
					        	echo "</td>";
					        	echo "</tr>";   
			        }

		        ?>   
            </table>
            <br/><br/>
        </form>
        

    <footer>
        <?php include("footer.php"); ?>
    </footer>
</body>
</html>