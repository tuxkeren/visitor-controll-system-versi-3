<?php
	
	date_default_timezone_set("Asia/Jakarta");
    $time = date('Y-m-d H:i:s');
	$id = $_POST['visitno'];
	$status = 0;
	
	include 'koneksi.php';
	$qry = "SELECT id FROM logs WHERE id= $id";
	$check = $db->query($qry) or die($db->error.__LINE__);

	if($check -> num_rows > 0){
		$sql = "UPDATE logs SET chkout=?, status=? WHERE id=?";
		$stat = $db->prepare($sql);

		$stat->bind_param('sii', $time, $status, $id);
		$stat->execute();

		$db->close();

		echo '<script type="text/javascript">
	    		window.alert("CHECK OUT SUCCESS, THANK YOU!");
	    		window.location="index.html";
			  </script>';
	}else{	
		echo '<script type="text/javascript">
	    		window.alert("THIS VISIT NUMBER IS NOT FOUND IN THE SYSTEM!");
	    		window.location="index.html";
			  </script>';
	}
?>