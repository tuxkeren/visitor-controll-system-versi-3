# Tentang Aplikasi #

Aplikasi ini adalah aplikasi untuk mengontrol tamu yang berkunjung ke perusahaan.
Saat ini digunakan di PT. H-Tech Oilfield Equipment.

Aplikasi ini menurupakan versi yang ke 3 dari aplikasi yang serupa, dimana penambahan 
fitur-fitur terbaru pada versi ini adalah sebagai berikut:

* Pembagian modul untuk security dan admin yang terpisah;
* Penyediaan fitur Booking Guest untuk tamu VIP atau asing;
* Penyediaan fitur merekam data tamu asing;
* Penyediaan Proses check in booking Guest;

### Komponen-komponen yang digunakan. ###

* PHP 7
* MySQL /MariaDB
* Webcam JS
* Bootstrap 3
* JDatatable
