<?php $thisPage = "staff"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
   
    <title><?php $thisPage ?></title>

    <!-- Styles -->
    <link href="../css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!--<link href="../css/jquery.dataTables.css" rel="stylesheet"> -->
    <link href="../css/dataTables.bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="#">
                        AVCMS
                    </a>
                </div>
                
                <?php 
                    // menu navigasi
                    include "menu-navigasi.php"; 
                ?>
                 
            </div>
        </nav>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="../admin/index.php">Dashboard</a></li>
                    <li class="active">Data Staffs</li>
                </ul>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title">Data Staffs</h2>
                    </div>
                    <div class="panel-body">
                        <a href="../admin/staff_new.php" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Staff</a><br/><br/>
                        <div class="table-responsive">
                            <table id="staff" class="table table-striped table-bordered" cellspacing="0" width="100%"> 
                                <thead>
                                    <tr>
                                        <th>NO.</th>
                                        <th>EMP ID.</th>
                                        <th>NAME</th>
                                        <th>DESEGNATION</th>
                                        <th>DEPT</th>
                                        <th>EXT.</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                    <?php
                                        include('../koneksi.php');
                                        
                                       
                                        $qry = "SELECT * FROM staff";
                                        $check = $db->query($qry) or die($db->error.__LINE__);	
                                        
                                        if($check -> num_rows > 0){
                                            $sql = "SELECT * FROM staff";
                                            if(!$result = $db->query($sql)){
                                                die('Query error [' .$db->error . ']');
                                            }
                                            
                                            $no = 1;
                                            while($staff = $result->fetch_object()){
                                                
                                                    echo "<tr>";
                                                        echo "<td>".$no."</td>";
                                                        echo "<td>".$staff->nik."</td>";
                                                        echo "<td>".$staff->name."</td>";
                                                        echo "<td>".$staff->desegnation."</td>";
                                                        echo "<td>".$staff->department."</td>";
                                                        echo "<td>".$staff->mobileext."</td>";
                                                        echo "<td align=\"center\">
                                                                <a href=\"staff_edit.php?nik=$staff->nik\" class=\"btn btn-sm btn-warning\"><span class=\"glyphicon glyphicon-pencil\"></span></a> 
                                                                <a href=\"staff_delete.php?nik=$staff->nik\" class=\"btn btn-sm btn-danger\"><span class=\"glyphicon glyphicon-remove\"></span></a>
                                                             </td>";
                                                    echo "</tr>";
                                               
                                                $no++;
                                            }

                                        }else{
                                            echo "<tr>";
                                            echo '<td colspan="10" align="center">';
                                            echo '<p style="\color: red;\">Data belum tersedia</p>';
                                            echo "</td>";
                                            echo "</tr>";   
                                        }
                                    ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>      

    <!-- Scripts -->
    <script src="../js/jquery-1.12.4.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.dataTables.min.js"></script>
    <script src="../js/dataTables.bootstrap.min.js"></script>
</body>
</html>

<script> 
    $(document).ready(function() {
        $('#staff').DataTable({});
    } );
</script>