<?php

$date = $_POST['date'];
$name = $_POST['name'];
$company = $_POST['company'];
$meet = $_POST['meet'];
$purpose = $_POST['purpose'];

include('../koneksi.php');

$sql = "INSERT INTO booking (visit_date,name,company,to_meet,purpose)
			  VALUES (?,?,?,?,?)";

$stat = $db->prepare($sql);
$stat->bind_param('sssss', $date,$name,$company,$meet,$purpose);
$stat->execute();
$db->close();

header("location:../admin/booking_guest.php");
