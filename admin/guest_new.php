<?php $thisPage = "guest"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
   
    <title><?php $thisPage ?></title>

    <!-- Styles -->
    <link href="../css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/jquery.dataTables.css" rel="stylesheet">
    <link href="../css/dataTables.bootstrap.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="#">
                        AVCMS
                    </a>
                </div>
                
                <?php 
                    // menu navigasi
                    include "menu-navigasi.php"; 
                ?>
                 
            </div>
        </nav>
    </div>
    <div class="container">
        <!-- Edit dan sesuaikan mulai dari sini -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="../admin/index.php">Dashboard</a></li>
                    <li><a href="../admin/guest.php">Foreigh Guests</a></li>
                    <li class="active">Record new Guest</li>
                </ul>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title">Record new Guest</h2>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="guest_insert.php" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="name">Full Name:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="name" name="name" autofocus required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="place_of_birth">Place of Birth:</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="place_of_birth" name="place_of_birth" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="date_of_birth">Date of Birth:</label>
                                <div class="col-sm-2">
                                    <input type="date" class="form-control" id="date_of_birth" name="date_of_birth" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="nationality">Nationality:</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="nationality" name="nationality" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="passwport_no">Passport No:</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="passwport_no" name="passport_no" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="date_issue">Date Issue:</label>
                                <div class="col-sm-2">
                                    <input type="date" class="form-control" id="date_issue" name="date_issue" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="date_expiry">Date Expiry:</label>
                                <div class="col-sm-2">
                                    <input type="date" class="form-control" id="date_expiry" name="date_expiry" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="company">Company:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="company" name="company" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="passport_pic">Passport Picture:</label>
                                <div class="col-sm-8">
                                    <input type="file" id="passport_pic" name="passport_pic">
                                    <p class="help-block">Upload gambar paspor yang sudah discan dari sini.</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="note">Note:</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" id="note" name="note" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                &nbsp;
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                  <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>
                                  <button type="reset" class="btn btn-danger"><span class="glyphicon glyphicon-repeat"></span> Reset</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Berakhir disini -->
    </div>      

    <!-- Scripts -->
    <script src="../js/jquery-3.1.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.dataTables.min.js"></script>
    <script src="../js/dataTables.bootstrap.min.js"></script>
</body>
</html>
