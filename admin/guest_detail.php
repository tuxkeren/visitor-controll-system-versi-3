<?php $thisPage = "guest"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
   
    <title><?php $thisPage ?></title>

    <!-- Styles -->
    <link href="../css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/jquery.dataTables.css" rel="stylesheet">
    <link href="../css/dataTables.bootstrap.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="#">
                        AVCMS
                    </a>
                </div>
                
                <?php 
                    // menu navigasi
                    include "menu-navigasi.php"; 
                ?>
                 
            </div>
        </nav>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Menu breadcrumb -->
                <ul class="breadcrumb">
                    <li><a href="../admin/index.php">Dashboard</a></li>
                    <li><a href="../admin/guest.php">Data Guests</a></li>
                    <li class="active">Guest Detail Info</li>
                </ul>
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title">Guest Detail Info</h2>
                    </div>
                    <?php
                    $id = $_GET['guest_id'];

                    include('../koneksi.php');

                    $qry = "SELECT * FROM fguest";
                    $check = $db->query($qry) or die($db->error.__LINE__);	
                    if($check -> num_rows > 0){
                        $sql = "SELECT * FROM fguest WHERE guest_id='$id'";
                        if(!$result = $db->query($sql)){
                            die('Query error [' .$db->error . ']');
                        }

                        while($guest = $result->fetch_object()){

                    ?>
                    <div class="panel-body">
                        <a href="../admin/guest_edit.php?guest_id=<?php echo $guest->guest_id; ?>" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
                        <a href="../admin/guest_delete.php?guest_id=<?php echo $guest->guest_id; ?>" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Delete</a><br/><br/>
                    <!-- Edit dan sesuaikan mulai dari sini -->
                    
                    <div class="col-sm-12">                        
                        <table  width=100% cellspacing="10" cellpadding="10">
                            <tr>
                                <td width="15%">Full Name</td><td width="20">:</td><td><strong><?php echo $guest->name; ?></strong></td>
                            </tr>
                            <tr>
                                <td width="15%">Place and Date Of Birth</td><td>:</td><td><strong><?php echo $guest->place_birth; ?>, <?php echo $guest->date_birth; ?></strong></td>
                            </tr>
                            <tr>
                                <td width="15%">Nationality</td><td>:</td><td><strong><?php echo $guest->nationality; ?></strong></td>
                            </tr>
                            <tr>
                                <td width="15%">Passport No.</td><td>:</td><td><strong><?php echo $guest->passport_no; ?></strong></td>
                            </tr>
                            <tr>
                                <td width="15%">Date Issued</td><td>:</td><td><strong><?php echo $guest->date_issue; ?></strong></td>
                            </tr>
                            <tr>
                                <td width="15%">Date Expiry</td><td>:</td><td><strong><?php echo $guest->date_expiry; ?></strong></td>
                            </tr>
                            <tr>
                                <td width="15%">Company</td><td>:</td><td><strong><?php echo $guest->company; ?></strong></td>
                            </tr>
                            <tr>
                                <td width="15%">Note</td><td>:</td><td rowspan="2" valign="top"><strong><?php echo $guest->note; ?></strong></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td><td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="15%">Passport</td><td>:</td><td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="15%">&nbsp;</td><td>&nbsp;</td><td><img class="img-responsive" src="../images/passports/<?php echo $guest->passport_foto ; ?>" alt="<?php echo $guest->passport_foto ; ?>" width="600"></td>
                            </tr>
                        </table>
                            
                    </div>
                       
                    <!-- Berakhir disini -->
                    </div>
                      <?php     
                    
                        }
                    };
                    ?>
                </div>
            </div>
        </div>
    </div>      

    <!-- Scripts -->
    <script src="../js/jquery-3.1.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.dataTables.min.js"></script>
    <script src="../js/dataTables.bootstrap.min.js"></script>
</body>
</html>
