<?php

$guest_name = $_POST['name'];
$place_of_birth = $_POST['place_of_birth'];
$date_of_birth = $_POST['date_of_birth'];
$nationality = $_POST['nationality'];
$passport_no = $_POST['passport_no'];
$date_issue = $_POST['date_issue'];
$date_expiry = $_POST['date_expiry'];
$company = $_POST['company'];
$note = $_POST['note'];

$temp = $_FILES['passport_pic']['tmp_name'];
$name = $_FILES['passport_pic']['name'];
$size = $_FILES['passport_pic']['size'];
$type = $_FILES['passport_pic']['type'];
$folder = "../images/passports/";

// upload gambar
if($type == 'image/jpg' or $type == 'image/png' or $type == 'image/jpeg'){
    move_uploaded_file($temp, $folder.$name);

    include('../koneksi.php');

    $sql = "INSERT INTO fguest (name,place_birth,date_birth,nationality,passport_no,date_issue,date_expiry,company,passport_foto,note)
                            VALUES (?,?,?,?,?,?,?,?,?,?)";

    $stat = $db->prepare($sql);
    $stat->bind_param('ssssssssss', $guest_name,$place_of_birth,$date_of_birth,$nationality,$passport_no,$date_issue,$date_expiry,$company,$name,$note);
    $stat->execute();
    $db->close();
}else{
    echo 'Tipe file salah.';
}

header("location:../admin/guest.php");