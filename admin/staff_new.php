<?php $thisPage = "staff"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
   
    <title><?php $thisPage ?></title>

    <!-- Styles -->
    <link href="../css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/jquery.dataTables.css" rel="stylesheet">
    <link href="../css/dataTables.bootstrap.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="#">
                        AVCMS
                    </a>
                </div>
                
                <?php 
                    // menu navigasi
                    include "menu-navigasi.php"; 
                ?>
                 
            </div>
        </nav>
    </div>
    <div class="container">
        <!-- Edit dan sesuaikan mulai dari sini -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="../admin/index.php">Dashboard</a></li>
                    <li><a href="../admin/staff.php">Data Staffs</a></li>
                    <li class="active">Record new staff</li>
                </ul>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title">Record new staff</h2>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="staff_insert.php" method="post">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="EmpID">Emp. ID:</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="EmpID" name="emp_id">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="name">Name:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="name" name="emp_name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="desegnation">Desgnation:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="desegnation" name="desegnation">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="dept">Department:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="dept" name="dept">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="ext">Extension:</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="ext" name="ext">
                                </div>
                            </div>
                            <div class="form-group">
                                &nbsp;
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                  <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-save"></span> Save</button>
                                  <button type="reset" class="btn btn-danger"><span class="glyphicon glyphicon-repeat"></span> Reset</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Berakhir disini -->
    </div>      

    <!-- Scripts -->
    <script src="../js/jquery-3.1.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.dataTables.min.js"></script>
    <script src="../js/dataTables.bootstrap.min.js"></script>
</body>
</html>
