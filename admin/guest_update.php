<?php

$id = $_POST['guest_id'];
$guest_name = $_POST['name'];
$place_birth = $_POST['place_of_birth'];
$date_birth = $_POST['date_of_birth'];
$nationality = $_POST['nationality'];
$passport_no = $_POST['passport_no'];
$date_issue = $_POST['date_issue'];
$date_expiry = $_POST['date_expiry'];
$company = $_POST['company'];
$note = $_POST['note'];

$temp = $_FILES['passport_pic']['tmp_name'];
$name = $_FILES['passport_pic']['name'];
$size = $_FILES['passport_pic']['size'];
$type = $_FILES['passport_pic']['type'];
$folder = "../images/passports/";

include('../koneksi.php');

if(empty($_FILES["passport_pic"]["tmp_name"])){
    
    $sql = "UPDATE fguest SET name = ?, place_birth = ?, date_birth = ?, nationality = ?, passport_no = ?, 
                              date_issue = ?, date_expiry = ?, company = ?, note = ? WHERE guest_id = ?";

    $stat = $db->prepare($sql);
    $stat->bind_param('sssssssssi', $guest_name,$place_birth,$date_birth,$nationality,$passport_no,$date_issue,$date_expiry,$company,$note,$id);
    $stat->execute();
    $db->close();
    
}else{
    
    if($type == 'image/jpg' or $type == 'image/png' or $type == 'image/jpeg'){
        // upload gambar
        move_uploaded_file($temp, $folder.$name);
        $sql = "UPDATE fguest SET name = ?, place_birth = ?, date_birth = ?, nationality = ?, passport_no = ?, 
                                  date_issue = ?, date_expiry = ?, company = ?, passport_foto = ?, note = ? WHERE guest_id = ?";

        $stat = $db->prepare($sql);
        $stat->bind_param('ssssssssssi', $guest_name,$place_birth,$date_birth,$nationality,$passport_no,$date_issue,$date_expiry,$company,$name,$note,$id);
        $stat->execute();
        $db->close();
        
    }else{
        echo 'Type File salah';
    }
   
}

header("location:../admin/guest.php");