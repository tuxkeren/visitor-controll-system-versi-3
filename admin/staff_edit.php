<?php $thisPage = "staff"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
   
    <title><?php $thisPage ?></title>

    <!-- Styles -->
    <link href="../css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/jquery.dataTables.css" rel="stylesheet">
    <link href="../css/dataTables.bootstrap.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="#">
                        AVCMS
                    </a>
                </div>
                
                <?php 
                    // menu navigasi
                    include "menu-navigasi.php"; 
                ?>
                 
            </div>
        </nav>
    </div>
    <div class="container">
        <!-- Edit dan sesuaikan mulai dari sini -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="../admin/index.php">Dashboard</a></li>
                    <li><a href="../admin/staff.php">Data Staffs</a></li>
                    <li class="active">Edit Data staff</li>
                </ul>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title">Edit Data Staff</h2>
                    </div>
                    
                    <?php
                    $empID = $_GET['nik'];

                    include('../koneksi.php');

                    $qry = "SELECT * FROM staff";
                    $check = $db->query($qry) or die($db->error.__LINE__);	
                    if($check -> num_rows > 0){
                        $sql = "SELECT * FROM staff WHERE nik='$empID'";
                        if(!$result = $db->query($sql)){
                            die('Query error [' .$db->error . ']');
                        }

                        while($staff = $result->fetch_object()){

                    ?>
                    
                    <div class="panel-body">
                        <form class="form-horizontal" action="staff_update.php" method="post">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="EmpID">Emp. ID:</label>
                                <div class="col-sm-3">
                                    <input type="hidden" name="nik" value="<?php echo $staff->nik; ?>">
                                    <input type="text" class="form-control" id="EmpID" name="emp_id" value="<?php echo $staff->nik; ?>" disabled="yes">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="name">Name:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="name" name="emp_name" value="<?php echo $staff->name; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="desegnation">Desgnation:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="desegnation" name="desegnation" value="<?php echo $staff->desegnation; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="dept">Department:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="dept" name="dept" value="<?php echo $staff->department; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="ext">Extension:</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="ext" name="ext" value="<?php echo $staff->mobileext; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                &nbsp;
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                  <button type="submit" class="btn btn-warning"><span class="glyphicon glyphicon-floppy-save"></span> Update</button>
                                  <button type="reset" class="btn btn-danger"><span class="glyphicon glyphicon-repeat"></span> Reset</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php     
                    
                        }
                    };
                    ?>
                </div>
            </div>
        </div>
        <!-- Berakhir disini -->
    </div>      

    <!-- Scripts -->
    <script src="../js/jquery-3.1.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.dataTables.min.js"></script>
    <script src="../js/dataTables.bootstrap.min.js"></script>
</body>
</html>
