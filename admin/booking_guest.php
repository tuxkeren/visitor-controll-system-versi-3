<?php $thisPage = "laporan-per-periode"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
   
    <title><?php $thisPage ?></title>

    <!-- Styles -->
    <link href="../css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!--<link href="../css/jquery.dataTables.css" rel="stylesheet"> -->
    <link href="../css/dataTables.bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="#">
                        AVCMS
                    </a>
                </div>
                
                <?php 
                    // menu navigasi
                    include "menu-navigasi.php"; 
                ?>
                 
            </div>
        </nav>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="../admin/index.php">Dashboard</a></li>
                    <li class="active">Booking Guests</li>
                </ul>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title">Booking Guests</h2>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table" width="100%">
                                <tr>
                                    <td>Date Visit</td><td>Guest Name</td><td>Company</td><td>To meet</td><td>Purpose</td><td>&nbsp;</td>
                                </tr>
                                <tr>
    								<form action="booking_insert.php" method="post">
    									<td><input type="date" class="form-control" name="date" placeholder="YYYY-MM-DD" required></td>
                                        <td><input list="guest" class="form-control" name="name" placeholder="Guest Name" required></td>
                                        <td><input list="company" class="form-control" name="company" placeholder="Company" required></td>                            
                                        <td><input list="staff" class="form-control" name="meet" placeholder="Who to meet" required></td>
                                        <td><input type="text" class="form-control" name="purpose" placeholder="Purpose" required></td>
                                        <td><button class="btn btn-sm-primary" type="submit">Book</button></td>
    								</form>																		
                                </tr>
                            </table>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>No.</th><th>Date Visit</th><th>Guest Name</th><th>Company</th><th>To meet</th><th>Purpose</th><th>&nbsp;</th>
                                </tr>
                                <tr>
                                    <?php
                                        include('../koneksi.php');
                                        
                                        $qry = "SELECT * FROM booking";
                                        $check = $db->query($qry) or die($db->error.__LINE__);	
                                        
                                        if($check -> num_rows > 0){
                                            $sql = "SELECT
                                                        b.id,
                                                        b.visit_date,
                                                        b.`name`,
                                                        b.company,
                                                        b.to_meet,
                                                        b.purpose,
                                                        s.`name` as staff_name,
                                                        s.nik
                                                    FROM
                                                        booking AS b ,
                                                        staff AS s
                                                    WHERE
                                                        b.to_meet = s.nik";
                                            if(!$result = $db->query($sql)){
                                                die('Query error [' .$db->error . ']');
                                            }
                                            
                                            $no = 1;
                                            while($booking = $result->fetch_object()){
                                                
                                                    echo "<tr>";
                                                        echo "<td>".$no."</td>";
                                                        echo "<td>".$booking->visit_date ."</td>";
                                                        echo "<td>".$booking->name."</td>";
                                                        echo "<td>".$booking->company."</td>";
                                                        echo "<td>".$booking->staff_name ."</td>";
                                                        echo "<td>".$booking->purpose ."</td>";
                                                        echo "<td align=\"center\">
                                                                <a href=\"booking_delete.php?id=$booking->id\" class=\"btn btn-sm btn-default\"><span class=\"glyphicon glyphicon-trash\"></span></a>
                                                             </td>";
                                                    echo "</tr>";
                                               
                                                $no++;
                                            }

                                        }else{
                                            echo "<tr>";
                                            echo '<td colspan="10" align="center">';
                                            echo '<font color="red">Data belum tersedia</font>';
                                            echo "</td>";
                                            echo "</tr>";   
                                        }
                                    ?>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>      

    <!-- Scripts -->
    <script src="../js/jquery-1.12.4.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.dataTables.min.js"></script>
    <script src="../js/dataTables.bootstrap.min.js"></script>
</body>
</html>

<datalist id="guest">
    <?php
        include('../koneksi.php');

        $sql = "SELECT * FROM fguest";
        $result = $db->query($sql);
        while($guest = $result->fetch_object()){
            echo "<option value=\"$guest->name \">";
        }
    ?>
</datalist>

<datalist id="staff">
    <?php
        $sql = "SELECT * FROM staff";
        $result = $db->query($sql);
        while($staff = $result->fetch_object()){
            echo "<option value=\"$staff->nik \">$staff->name</option>";
        }
    ?>
</datalist>
