<div class="collapse navbar-collapse" id="app-navbar-collapse">
    <!-- Left Side Of Navbar -->
    <ul class="nav navbar-nav">
        <li<?php if($thisPage === "dashboard") echo ""; ?>><a href="../admin/index.php">Dashboard</a></li>
        <li<?php if($thisPage === "staff") echo ""; ?>><a href="../admin/staff.php">Data Staff</a></li>
        <li<?php if($thisPage === "guest") echo ""; ?>><a href="../admin/guest.php">Tamu Asing</a></li>
        <li<?php if($thisPage === "book") echo ""; ?>><a href="../admin/booking_guest.php">Book Checkin</a></li>
        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Laporan<span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li<?php if($thisPage === "rpt-onsite") echo ""; ?>><a href="../reports/rpt-onsite.php">On Site Visitor</a></li>
                <li<?php if($thisPage === "rpt-leave") echo ""; ?>><a href="../reports/rpt-leave.php">Leave Visitor</a></li>
                <li<?php if($thisPage === "rpt-tasing") echo ""; ?>><a href="../reports/rpt-tasing.php">Tamu Asing</a></li>
                <li<?php if($thisPage === "rpt-perpriode") echo ""; ?>><a href="../reports/rpt-perpriode.php">Tamu Per Periode</a></li>
                <li<?php if($thisPage === "rpt-asingperpriode") echo ""; ?>><a href="../reports/rpt-asingperpriode.php">Tamu Asing Per Periode</a></li>
            </ul>
        </li>
    </ul>

    <!-- Right Side Of Navbar -->
    <ul class="nav navbar-nav navbar-right">
        <!-- Authentication Links 
            <li><a href="#">Login</a></li>
            <li><a href="#">Daftar</a></li>-->
    </ul>
</div>